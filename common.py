# -*- coding:utf-8 -*-

from flask import jsonify

def make_response(data = None, msg = 'OK', status = 0, http_status = 200):
    response = jsonify({
        'data': data,
        'status': status,
        'msg': msg
    })
    response.status_code = http_status
    return response