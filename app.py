# -*- coding: utf-8 -*-

from flask import Flask
import config, requests
from common import make_response

def create_app():
    app = Flask(__name__, static_url_path='')

    @app.route('/getArangodb')
    def get_data():
        url = config.ARANGODB
        query = '''
        for u in company
            limit 10
            return u
        '''
        # r=requests.post(
        #http://172.17.186.45:18531/_db/poc_data_20170115/_api/cursor
        #     url='http://localhost:8529/_db/_system/_api/cursor',
        #     json={'query': query, 'bindVars': {'name':'df'}}
        # )
        r = requests.post(
            url='http://root@localhost:8529/_db/_system/_api/cursor',
            json={'query': query, 'bindVars': {}},
            headers={
                'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE0ODkwMzk0NTIsImlzcyI6ImFyYW5nb2RiIiwicHJlZmVycmVkX3VzZXJuYW1lIjoicm9vdCJ9.aJmxpKfEgOauOVTDrDJ-ooICNIr953H8jdTkvE9tlB8='
            }
        )
        # r_obj = r.json()
        data = r.json()
        return make_response(
            data=data
        )

    return app