# -*- coding: utf-8 -*-

import config

from app import create_app

application = create_app()

if __name__ == '__main__':
    application.run(
        host=config.SERVER.get('host', '0.0.0.0'),
        port=config.SERVER.get('port', '5555'),
        debug=True
    )